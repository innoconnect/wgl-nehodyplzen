function init() {
    const data = new DataLoader();
    data.loadPosData("data/data.json");
}

function visualize(data){

    WGL.init(data.num,'lib/','map', true);

    map.on("move", onMove);
    map.on("zoomend", () => $(".link-permalink").trigger("permalink:change"));
    map.on("moveend", () => $(".link-permalink").trigger("permalink:change"));
    map.on("load", () => {
        $("#draw-polygon").click();
        $("#draw-polygon").click();
    });

    window.onresize = onResize;

    const heatmap = WGL.addHeatMapDimension(data.pts, 'heatmap', 1);
    heatmap.radiusFunction = function (r, z) {
        return r*(z/10);
    };
    heatmap.setRadius(3);
    heatmap.setVisible(true);

    // use normal distribution for values around point
    heatmap.gauss = true;

    addHeatMapControl(heatmap,'chm');

    const mapdim = WGL.addMapDimension(data.pts, 'themap');
    mapdim.setVisible(false);
    mapdim.pointSize = function(z){
      //return 10 * 6.388019799072483e-06 * Math.pow(2, z);
      return 10;
    };

    WGL.addPolyBrushFilter('themap','polybrush');

    WGL.addExtentFilter();

    const updatedraw = (mode, loadedFeatures) => {

        if(loadedFeatures && loadedFeatures.length > 0) {
            let loadedFeaturesJson = JSON.parse(loadedFeatures);
            let loadedFeature;
            for(let f in loadedFeaturesJson) {
                loadedFeature = {};
                loadedFeature['type'] = "Polygon";
                loadedFeature['coordinates'] = [loadedFeaturesJson[f]];
                draw.add(loadedFeature);
            }
        }

        const features = draw.getAll();
        if (features.features.length > 0){
            let polygons = [];
            features.features.forEach((f)=>{
                if (validity(f)){
                    try{
                        polygons[f.id] = geometryToPoly(f);
                    }
                    catch(e){

                    }
                }
            });
            polygons.length = Object.keys(polygons).length;
            if(polygons.length > 0) {
                $("#chd-container-footer-area").removeClass("hide");
                $(".active-filters-container").slideDown();
            } else {
                return;

            }
            WGL.filterDim('themap','polybrush', polygons);
        }
        else{
            WGL.filterDim('themap', 'polybrush', []);
            $("#chd-container-footer-area").addClass("hide");
        }
    };
    map.on('draw.create', () => { updatedraw(); $(".link-permalink").trigger("permalink:change"); });
    map.on('draw.delete', () => { updatedraw(); $(".link-permalink").trigger("permalink:change"); });

    map.on('draw.update', updatedraw);

    map.on('draw.render', updatedraw);

    let loadedFeatures = getUrlParameter("map");
    if(loadedFeatures !== "") {
        updatedraw({}, loadedFeatures);
    }

    updateIntro(data.start_date, data.end_date, data.num);

    buildCharts(data, "red");

    WGL.initFilters();

    let pw = setPointSelection();

    $("#permalink-input").val(window.location.href);

    $(".link-permalink").on("permalink:change", () => {
        if(!$("#field-permalink").hasClass("hide")) {
            $("#field-permalink").animate({width: 'toggle'}, 100).toggleClass("hide");
        }
        $("input[type=text].text-permalink").val("");
        $(".spinner-permalink").addClass("hide");
    });

    $(".link-permalink").click(() => {

        const list_of_charts = $(".chart-content");

        for(let i=0; i<list_of_charts.length; i++) {
            $(list_of_charts[i]).trigger("chart:update-permalink");
        }

        let chartsStatus = Array.prototype.slice.call(document.querySelectorAll("[id^=min].chart-header")).map((a) => $(a).hasClass("btn-plus") ? 0 : 1);

        $(".mapboxgl-canvas").trigger("popup:update-permalink");

        let newURL = $("#permalink-input").val();

        newURL = updateURLParameter(newURL, "zoom", map.getZoom());
        newURL = updateURLParameter(newURL, "center", map.getCenter().lng + ","+map.getCenter().lat);
        newURL = updateURLParameter(newURL, "charts", chartsStatus);
        newURL = updateURLParameter(newURL, "layers", [(WGL.getDimension('heatmap').getVisible() ? 1 : 0), (WGL.getDimension('idt').getEnabled() ? 1 : 0)].toString());
        newURL = updateURLParameter(newURL, "radius", $("#slider_radius").val());
        newURL = updateURLParameter(newURL, "scheme", $(".color-scheme-selected").data("scheme"));
        newURL = updateURLParameter(newURL, "lang", $(".language-select").val());

        let zoom_btns_selected = $("[id^=ch].legend-scale.select-legend-scale");
        let zoom_btns = [];
        for(let i=0; i<zoom_btns_selected.length; i++) {
            zoom_btns.push(zoom_btns_selected[i].id);
        }

        if(zoom_btns !== "") {
            newURL = updateURLParameter(newURL, "zoom_btns", zoom_btns.join(","));
        }

        let mapDimension;
        for(let dimension in WGL._dimensions) {
            if(WGL._dimensions[dimension] instanceof WGL.dimension.MapDimension) {
                mapDimension = WGL._dimensions[dimension];
                break;
            }
        }

        if(draw.getAll()['features'].length > 0) {

            let polybrushFilter = "[";
            let featuresArray = draw.getAll()['features'];

            for(let features=0; features < featuresArray.length; features++) {
                polybrushFilter += "[";

                for(let geometry = 0; geometry < featuresArray[features]["geometry"]["coordinates"].length; geometry++) {
                    for (let point = 0; point < featuresArray[features]["geometry"]["coordinates"][geometry].length; point++) {

                        if(featuresArray[features]["geometry"]["coordinates"][geometry][point] == null) {
                            return;
                        }

                        polybrushFilter += "[" + featuresArray[features]["geometry"]["coordinates"][geometry][point].toString() + "]";

                        if (point < featuresArray[features]["geometry"]["coordinates"][geometry].length - 1) {
                            polybrushFilter += ",";
                        }
                    }
                    if (geometry < featuresArray[features]["geometry"]["coordinates"].length - 1) {
                        polybrushFilter += ",";
                    }
                }

                polybrushFilter += "]";
                if(features < featuresArray.length-1) {
                    polybrushFilter += ",";
                }
            }

            polybrushFilter += "]";

            newURL = updateURLParameter(newURL, mapDimension.name, polybrushFilter);
        } else {
            newURL = updateURLParameter(newURL, mapDimension.name, "");
        }

        newURL = window.location.origin + window.location.pathname + '?' + newURL;

        $.ajax({
            type: "POST",
            url: "https://go.innoconnect.net/api/url/submit",
            headers: {"X-API-Key": "vZI4PlBXDpE753WMfNyotvaGz6xEg4KfkkDjzJQJ", "Content-Type" : "application/x-www-form-urlencoded"},
            data: {"target": newURL},
            beforeSend: () => {
                $("input[type=text].text-permalink").val("");
                $("input[type=text].text-permalink").addClass("unselectable");
                $(".spinner-permalink").removeClass("hide");
            },
            success: data => {
                $("input[type=text].text-permalink").val(data['shortUrl']);
                $("input[type=text].text-permalink").removeClass("unselectable");
                $(".spinner-permalink").addClass("hide");
            },
            error: () => $("input[type=text].text-permalink").val("")
        });


        $("#field-permalink").animate({width: 'toggle'}, 100).toggleClass("hide");
        if(!$("#field-permalink").hasClass("hide")) {
            let el = document.querySelector(".text-permalink");
            el.focus();
            el.select();
        }
    });

    $(".copy-permalink").click(() => {
        let el = document.querySelector(".text-permalink");
        el.focus();
        el.select();
        document.execCommand('copy');
    });

    $("#min-about").on("click", function() {
        $("#min-about").toggleClass("btn-plus");
        $($("#about")[0]).slideToggle();

        if($("#about-chevron").text() === "keyboard_arrow_down") {
            $("#about-chevron").text("keyboard_arrow_up");
            $("#right").scrollTop(0);
        } else {
            $("#about-chevron").text("keyboard_arrow_down");
        }
    });

    $(".chart-filters-area i").click(function() {
        draw.deleteAll();
        draw.changeMode(draw.modes.SIMPLE_SELECT);
        if(WGL.getDimension('themap').getVisible()
            && draw.getAll().features.length === 0) {
            const idt = WGL.getDimension('idt');
            idt.setEnabled(true);
        }
        $("#controlsToggle .btn-selected").removeClass("btn-selected");
        WGL.filterDim('themap', 'polybrush', []);
        $("#chd-container-footer-area").addClass("hide");
        if($(".active-filters-container [id^=chd-container-footer]:not(.hide)").length > 0) {
            $(".close-item-filters").removeClass("hide");
            $("#active-filters-placeholder").addClass("hide");
            $(".active-filters-item .bar-item").addClass("bar-item-active");
            $(".active-filters-container").slideDown();
        } else {
            $("#active-filters-placeholder").removeClass("hide");
            $(".close-item-filters").removeClass("hide");
        }
    });
    $(".close-item-btn").click(function() {
        $(this).hide();
        $(this).parent().slideToggle();
        draw.changeMode(draw.modes.SIMPLE_SELECT);
        $(this).parent().parent().find(".bar-item").toggleClass("bar-item-active");
    });
    $(".active-filters-item").click(function() {
        $(".active-filters-container").slideToggle();
        $(".active-filters-container .close-item-btn").show();
        $(this).children(".bar-item").toggleClass("bar-item-active");
    });
    $(".area-item").click(function(){
        $(".controls-container").slideToggle();
        $(".controls-container .close-item-btn").show();
        $(this).children(".bar-item").toggleClass("bar-item-active");
    });
    $(".layers-item").click(function(){
        $(".layers-container").slideToggle();
        $(".layers-container .close-item-btn").show();
        $(this).children(".bar-item").toggleClass("bar-item-active");
    });
    $(".heatmap-item").click(function() {
        $(".heatmap-controls-container").slideToggle();
        $(".heatmap-controls-container .close-item-btn").show();
        $(this).children(".bar-item").toggleClass("bar-item-active");
    });

    $(".charts-item.navbar-item").click(function(){

        $("#right").toggle();
        $(this).toggleClass("active");
        $("#map").toggleClass("fullscreen");
        $(".charts-item .bar-item").toggleClass("bar-item-active");

        onResize();
        map.resize();

        updateChartsVisibility();
    });

    $(".active-filters-item").mouseover(function(){
        $(this).children(".bar-item").addClass("bar-item-hover");
    });

    $(".area-item").mouseover(function(){
        $(this).children(".bar-item").addClass("bar-item-hover");
    });

    $(".layers-item").mouseover(function(){
        $(this).children(".bar-item").addClass("bar-item-hover");
    });

    $(".heatmap-item").mouseover(function() {
        $(this).children(".bar-item").addClass("bar-item-hover");
    });
    $(".charts-item").mouseover(function() {
        $(this).children(".bar-item").addClass("bar-item-hover");
    });

    $(".export-pdf").click(toCanvas);

    $(".active-filters-item").mouseout(function(){
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });
    $(".area-item").mouseout(function(){
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });
    $(".layers-item").mouseout(function(){
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });
    $(".heatmap-item").mouseout(function() {
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });
    $(".charts-item").mouseout(function() {
        $(this).children(".bar-item").removeClass("bar-item-hover");
    });

    WGL.render();

    $("#points_visible").click(function(){
        $(this).toggleClass("layer-selected");
        const selected = $(this).hasClass("layer-selected");
        const l = WGL.getDimension(this.name);
        l.setVisible(selected);
        const idt = WGL.getDimension('idt');
        idt.setEnabled(selected);
        if(!selected) {
            $("#wgl-win-close").click();
        }
        WGL.render();

        if($(".link-permalink").length > 0) {
            $(".link-permalink").trigger("permalink:change");
        }
    });
    $("#heatmap_visible").click(function(){
        $(this).toggleClass("layer-selected");
        const selected = $(this).hasClass("layer-selected");
        const l = WGL.getDimension(this.name);
        l.setVisible(selected);
        WGL.render();

        if($(".link-permalink").length > 0) {
            $(".link-permalink").trigger("permalink:change");
        }
    });

    const layersActive = getUrlParameter("layers").split(",");
    if(parseInt(layersActive[0]) === 0) {
        $("#heatmap_visible").click();
    }
    if(parseInt(layersActive[1]) === 1) {
        $("#points_visible").click();
    }

    let languageActive = getUrlParameter("lang");
    if(languageActive !== "") {
        selectLanguage(languageActive);
    } else {
        selectLanguage('en');
    }

    $(".control-btn").click(function() {
        toggleControl(this);
    });

    $("#loading").addClass("hidden");

    onMove();

    const charts_element = $("#charts");
    charts_element.sortable(
        {
            placeholder: "ui-state-highlight",
            handle: '.chart-header',
            zIndex: 9999,
            helper: 'clone',
            cursor: "move",
            start: function() {
                $(this).find(".chart-header").addClass('grabbing');
            },
            stop: function() {
                $(this).find(".chart-header").removeClass('grabbing');
            },
            update: ( event, ui ) => {

                const idPreviousSibling = $($(ui.item[0]).prev()).attr("id");

                $("#chd-container-"+$($(ui.item[0]).children(".chart-content")[0]).attr("id") +" .chart-content").css("visibility", "visible");

                const element = $("#chd-container-footer-"+$($(ui.item[0]).children(".chart-content")[0]).attr("id")).detach();
                if(typeof idPreviousSibling !== "undefined") {
                    $("#chd-container-footer-"+$("#"+idPreviousSibling).children(".chart-content").attr("id")).after(element);
                } else {
                    $("#filters-container").prepend(element);
                }

                updateChartsVisibility();
            }
        }
    );
    updateChartsVisibility();
    $("#right").on("scroll", () => updateChartsVisibility());

    let loadedCharts = getUrlParameter("charts");
    if(loadedCharts !== "") {
        loadedCharts = loadedCharts.split(",");
        let listOfCharts = document.querySelectorAll("[id^=min].chart-header");
        for (let c in listOfCharts) {
            if(loadedCharts[c] === "0") {
                $(listOfCharts[c]).click();
            }
        }
    }

    let zoom_btns = getUrlParameter("zoom_btns");
    if(zoom_btns !== "") {
        zoom_btns = zoom_btns.split(",");
        for(let i=0; i<zoom_btns.length; i++) {
            $("#"+zoom_btns[i]+" i").click();
        }
    }
}

function resize(){
    WGL.getManager().updateMapSize();
    WGL.mcontroller.resize();
    WGL.mcontroller.zoommove(map.getZoom(), getTopLeftTC());
    WGL.render();
}

function getTopLeftTC() {
    const ZERO_PIX_3857_COEF = 128/20037508.34;
    const z = map.getZoom() + 1;
    const scale = Math.pow(2, z);
    const dx = WGL.getManager().w/2/scale;
    const dy = WGL.getManager().h/2/scale;

    const TL3857_ZERO = {x: -20037508.34, y: 20037508.34};
    const c = map.getCenter();

    const proj = new SphericalMercator.SphericalMercator();
    const center_3857 = proj.forward([c.lng, c.lat]);

    return {
        x: (center_3857[0] - TL3857_ZERO.x)*ZERO_PIX_3857_COEF - dx,
        y: (-center_3857[1] + TL3857_ZERO.y)*ZERO_PIX_3857_COEF - dy
    };
}

function onMove() {
    const z = map.getZoom() + 1;
    WGL.mcontroller.zoommove(z, getTopLeftTC());
}

function onResize() {
    WGL.getManager().updateMapSize();
    WGL.mcontroller.resize();
    onMove();
}

function updateIntro(start_date, end_date, num) {
    $("#start_date b").html(start_date);
    $("#end_date b").html(end_date);
    $("#no_crimes b").html(num);
}

function buildCharts(data) {

    const charts = [];

    const involvement = {name: "involvement", data: data.flagsArray, flags: data.flagsEnum};
    const chd0 = new WGL.ChartDiv("charts", "ch0", "involvement", "involvement", 5);
    chd0.setDim(WGL.addFlagsDimension(involvement));
    WGL.addFlagsFilter(involvement.name, "involvementF");
    charts['involvement'] = new WGL.ui.FlagsCheckboxes(involvement, "ch0", "involvementF", data.flagsEnum, data.flagsNames, {
        h: 145,
        permalink_input: "permalink-input"
    });
    new WGL.FilterListChartDiv("filters-container", charts['involvement'], "involvement", 5);

    const date = {data: data.date, domain: data.dateEnum, name: "date", type: "ordinal", label: "date"};
    const chd1 = new WGL.ChartDiv("charts", "ch1", "date", "Date", 53);
    chd1.setDim(WGL.addOrdinalHistDimension(date));
    WGL.addLinearFilter(date, 53, "dateF");
    const dateParams = {
        h: 300,
        margin: {
            top: 40,
            right: 70,
            bottom: 80,
            left: 60
        },
        rotate_x: true,
        numbers_formatting: "d",
        permalink_input: "permalink-input"
    };
    charts['date'] = new WGL.ui.StackedBarChart(date, "ch1", "Date (month - year)", "dateF", dateParams);
    charts['date'].xformat = function (a) {
        if (a.startsWith('01') || a.startsWith('07')) {
            return a;
        }
    };
    new WGL.FilterListChartDiv("filters-container", charts['date'], "date", 53);

    /* MONTHS */
    const months = {
        data: data.months,
        domain: data.monthsArray,
        name: 'month of the year',
        type: 'ordinal',
        label: "month of the year"
    };
    const chd2 = new WGL.ChartDiv("charts", "ch2", "month of the year", "Month of the Year", 12);
    chd2.setDim(WGL.addOrdinalHistDimension(months));
    WGL.addLinearFilter(months, 12, 'monthsF');
    const paramsMonths = {
        w: 500,
        h: 300,
        margin: {
            top: 40,
            right: 70,
            bottom: 80,
            left: 60
        },
        numbers_formatting: "d",
        rotate_x: true,
        permalink_input: "permalink-input"
    };
    charts['month of the year'] = new WGL.ui.StackedBarChart(months, "ch2", "Month of the year", 'monthsF', paramsMonths);
    new WGL.FilterListChartDiv("filters-container", charts['month of the year'], "month of the year", 12);

    /* DAYS*/
    const days = {
        data: data.days,
        domain: data.daysarray,
        name: 'day of the week',
        type: 'ordinal',
        label: "day of the week"
    };
    const chd3 = new WGL.ChartDiv("charts", "ch3", "day of the week", "Day of the week", 7);
    chd3.setDim(WGL.addOrdinalHistDimension(days));
    WGL.addLinearFilter(days, 7, 'daysF');
    const paramsDays = {
        w: 500,
        h: 300,
        margin: {
            top: 30,
            right: 70,
            bottom: 80,
            left: 60
        },
        numbers_formatting: "d",
        rotate_x: true,
        permalink_input: "permalink-input"
    };
    charts['day of the week'] = new WGL.ui.StackedBarChart(days, "ch3", "Day of the week", 'daysF', paramsDays);
    new WGL.FilterListChartDiv("filters-container", charts['day of the week'], "day of the week", 7);

    /*HOURS*/
    const hours = {
        data: data.hours,
        domain: data.hoursEnum,
        name: 'hour of the day',
        type: 'ordinal',
        label: "hour of the day"
    };
    const chd4 = new WGL.ChartDiv("charts", "ch4", "hour of the day", "Hour of the day", 24);
    chd4.setDim(WGL.addOrdinalHistDimension(hours));
    WGL.addLinearFilter(hours, 24, 'hoursF');
    const paramsHour = {
        numbers_formatting: "d",
        permalink_input: "permalink-input"
    };
    charts['hour of the day'] = new WGL.ui.StackedBarChart(hours, "ch4", "hour of the day", 'hoursF', paramsHour);
    new WGL.FilterListChartDiv("filters-container", charts['hour of the day'], "hour of the day", 24);

    const severity = {
        data: data.severity,
        domain: data.severityEnum,
        name: 'severity',
        type: 'ordinal',
        label: "severity"
    };
    const chd5 = new WGL.ChartDiv("charts", "ch5", "severity", "Severity *", 3);
    chd5.setDim(WGL.addOrdinalHistDimension(severity));
    WGL.addLinearFilter(severity, 3, 'severityF');
    const paramsSeverity = {
        h: 300,
        margin: {
            top: 40,
            right: 70,
            bottom: 140,
            left: 60
        },
        rotate_x: false,
        numbers_formatting: "d",
        text: "* The severity has been derived from the Police’s real-time accidents reports, using an automated text analysis. It might thus differ from the official statistics.",
        permalink_input: "permalink-input"
    };
    charts['severity'] = new WGL.ui.StackedBarChart(severity, "ch5", "Severity", 'severityF', paramsSeverity);
    new WGL.FilterListChartDiv("filters-container", charts['severity'], "severity", 3);

    //identify pop-up window
    const idt = WGL.addIdentifyDimension(data.pts, data.pts_id, 'idt', "data/identify/");
    idt.onlySelected = true;
    idt.pointSize = 10;
    idt.setEnabled($("#points_visible").hasClass("layer-selected"));

    WGL.addCharts(charts);
}

function setPointSelection(languageData) {

    let pw;
    if(typeof languageData === "undefined") {

        // point selection
        pw = new WGL.ui.PopupWin(".mapboxgl-canvas", "idt", "Accident Details");
        pw.configurePermalinkInput("permalink-input");
        pw.setProp2html(function (t) {

            const weekarray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

            const d = (Date.parseExact(t["od"], "yyyy-MM-dd HH:mm:ss"));
            const wd = weekarray[d.getDay()];
            const hour = ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
            let severity;
            if (t["ts_flags"].indexOf("death") !== -1) {
                severity = "fatal";
            } else if (t["ts_flags"].indexOf("injury") !== -1) {
                severity = "injuries";
            } else {
                severity = "no injury";
            }
            const description = t["popis"];

            let s = "<table>";
            s += "<tr><td width='140px'>Date: </td><td>" + d.toLocaleDateString('en-US', {
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            }) + "</td></tr>";
            s += "<tr><td>Weekday: </td><td>" + wd + "</td></tr>";
            s += "<tr><td>Time: </td><td>" + hour + "</td></tr>";
            s += "<tr><td>Severity: </td><td style='text-transform: capitalize'>" + severity + "</td></tr>";
            s += "<tr><td>Description: </td><td style='word-wrap: normal; white-space: normal; max-width: 200px'>" + description + "</td></tr>";

            return s;
        });

    } else {
        // point selection
        pw = new WGL.ui.PopupWin(".mapboxgl-canvas", "idt", languageData['popup-details-title']);
        pw.setProp2html(function (t) {

            const weekarray = languageData['weekdays-array'];

            const d = (new Date(t["od"]));
            const wd = weekarray[d.getDay()];
            const hour = ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
            let severity;
            if (t["ts_flags"].indexOf("death") !== -1) {
                severity = languageData['level-severity-fatal'];
            } else if (t["ts_flags"].indexOf("injury") !== -1) {
                severity = languageData['level-severity-injuries'];
            } else {
                severity = languageData['level-severity-no-injury'];
            }
            const description = t["popis"];

            let s = "<table>";
            s += "<tr><td width='140px'>"+languageData['popup-details-date']+": </td><td>" + d.toLocaleDateString(languageData['locale'], {
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            }) + "</td></tr>";
            s += "<tr><td>"+languageData['popup-details-weekday']+": </td><td>" + wd + "</td></tr>";
            s += "<tr><td>"+languageData['popup-details-time']+": </td><td>" + hour + "</td></tr>";
            s += "<tr><td>"+languageData['popup-details-severity']+": </td><td style='text-transform: capitalize'>" + severity + "</td></tr>";
            s += "<tr><td>"+languageData['popup-details-description']+": </td><td style='word-wrap: normal; white-space: normal; max-width: 200px'>" + description + "</td></tr>";

            return s;
        });
    }

    pw.setMovemap(function (dx, dy) {
        let c = map.getCenter();
        const cpx = map.project(c);
        cpx.x -= dx;
        cpx.y -= dy;
        map.setCenter(map.unproject(cpx));
    });
    map.on("move", function () {
        pw.zoommove();
    });
    pw.loadFilters();

    return pw;
}

function addHeatMapControl(hm,divid){

    let radius = getUrlParameter("radius");
    if(radius === "") {
        radius = 3;
    } else {
        radius = parseInt(radius);
    }

    let scheme = getUrlParameter("scheme");

    const thediv = $("#"+divid);
    thediv.append(
        "<div class='hm-label'>"+
        "<div class='hm-radius-title'>" +
        "<text style='display: table-cell'>Radius </br><span style='font-size: 8pt'>(metres)</span></text><text id='radius_label'></text>"+
        "</div>" +
        '<div class="range-control">' +
        '<input id="slider_radius" type="range" min="0.1" max="10" step="0.3" value="'+radius+'" data-thumbwidth="20">' +
        '<output name="rangeVal" for="points" style="left: calc('+(radius*9.3)+'% - -8px)">'+radius+'</output>' +
        '</div>' +
        "</div>");
    thediv.append(
        "<div class='hm-label'>"+
        "<div class='hm-density-title'>" +
        "<text display='table-cell; vertical-align: middle'>Density </br><span style='font-size: 8pt'>of records within</br>the radius</span></text><text id='radius_label'></text>"+
        "</div>" +
        "<div id='heatmap-legend' style='display: table-cell; vertical-align: middle; float: right'></div>"+
        "</div>"
    );

    WGL.addColorFilter(hm.id,'colorbrush');
    const legend = new  WGL.ui.HeatMapLegend("heatmap-legend", 'colorbrush', true);
    hm.addLegend(legend);

    $("#slider_radius").on("input", function(){

        hm.setRadius(this.value);

        const control = $(this),
            controlMin = control.attr('min'),
            controlMax = control.attr('max'),
            controlVal = control.val(),
            controlThumbWidth = control.data('thumbwidth');

        const range = controlMax - controlMin;

        const position = ((controlVal - controlMin) / range) * 100;
        const positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
        const output = control.next('output');

        output
            .css('left', 'calc(' + position + '% - ' + positionOffset + 'px)')
            .text(controlVal);

        WGL.render();

        if($(".link-permalink").length > 0) {
            $(".link-permalink").trigger("permalink:change");
        }
    });

    $(".color-scheme-btn").click((evt) => {
        toggleColorScheme(evt.target.closest(".color-scheme-btn"));
        $("#heatmap-legend").empty();
        const legend = new  WGL.ui.HeatMapLegend("heatmap-legend", 'colorbrush', true);
        hm.addLegend(legend);
        WGL.render();

        if($(".link-permalink").length > 0) {
            $(".link-permalink").trigger("permalink:change");
        }
    });

    if(scheme !== "") {
        toggleColorScheme(document.querySelector(".color-scheme-btn[data-scheme="+scheme+"]"));
        $("#heatmap-legend").empty();
        const legend = new  WGL.ui.HeatMapLegend("heatmap-legend", 'colorbrush', true);
        hm.addLegend(legend);
        WGL.render();
    }
}

function updateChartsVisibility() {
    let dimension_element;
    for(let dimension in WGL._dimensions) {
        dimension_element = WGL._dimensions[dimension];
        if(dimension_element instanceof WGL.dimension.HistDimension) {
            let element = $("[data-name='"+dimension+"']");

            if(dimension_element.visible && !visibleY(element[0])) {
                dimension_element.setVisible(false);
            }

            if(!dimension_element.visible && visibleY(element[0])) {
                dimension_element.setVisible(true);
                WGL.render();
            }
        }
    }
}

function wgsToZeroLevel(pos){
    const proj = new SphericalMercator.SphericalMercator();
    const point = proj.forward(pos);
    const x = (point[0] + 20037508.34) / (20037508.34*2)*256;
    const y = -(point[1] - 20037508.34) / (20037508.34*2)*256;
    return {x: x, y: y}
}

function geometryToPoly(feature) {
    const geom = feature.geometry.coordinates[0];

    let poly_zero = [];
    for(let i = 0;i < geom.length-1; i++){
        poly_zero.push(wgsToZeroLevel(geom[i]));
    }
    // triangulate
    const ts = new poly2tri.SweepContext(poly_zero);
    ts.triangulate();
    return trianglesToArray(ts.getTriangles());
}

function trianglesToArray(trig) {
    let points = [];
    for ( let i in trig) {
        for ( let j in trig[i].points_) {
            points.push(trig[i].points_[j].x);
            points.push(trig[i].points_[j].y);
        }
    }
    return points;
}

function validity(feature){
    if (feature.geometry.coordinates.length === 0){
        return false;
    }
    const geom = feature.geometry.coordinates[0];
    if (geom.length > 3){
        for(let i = 1;i < geom.length-1; i++){
            if (geom[i][0] === geom[i-1][0] && geom[i][1] === geom[i-1][1]){
                return false;
            }
        }
        return true
    }
    return false;
}

function selectLanguage(language) {
    const data = new DataLoader();
    data.loadLanguageData('data/languages/'+language+'.json');
}

function renderLanguage(languageData) {

    $(".language-select").val(languageData['language']);

    document.title = capitalizeString(languageData['about-title']);

    $("#about").html(languageData['about']);
    $(".active-filters-item-title").html(languageData['navitem-active-filters']);
    $(".heatmap-item-title").html(languageData['navitem-heatmap']);
    $(".layers-item-title").html(languageData['navitem-layers']);
    $(".area-item-title").html(languageData['navitem-select-area']);
    $(".charts-item-title").html(languageData['navitem-charts']);
    $("#active-filters-placeholder").html(languageData['empty-filters-msg']);
    $(".heatmap-controls-color").html(languageData['heatmap-controls-color']);
    $(".hm-radius-title").html(languageData['heatmap-controls-radius']);
    $(".hm-density-title").html(languageData['heatmap-controls-density']);
    $("#heatmap_visible").html(languageData['layers-heatmap']);
    $("#points_visible").html(languageData['layers-points']);

    $("#chd-container-about .chart-title text").text(languageData['about-title']);
    $("#chd-container-ch0 .chart-title text").text(languageData['flags-involvement-title']);
    $("#chd-container-ch1 .chart-title text").text(languageData['chart-date-title']);
    $("#chd-container-ch2 .chart-title text").text(languageData['chart-month-title']);
    $("#chd-container-ch3 .chart-title text").text(languageData['chart-day-title']);
    $("#chd-container-ch4 .chart-title text").text(languageData['chart-hour-title']);
    $("#chd-container-ch5 .chart-title text").text(languageData['chart-severity-title']);

    $("#chd-container-footer-ch0 .chart-title-footer text").text(languageData['flags-involvement-title']);
    $("#chd-container-footer-ch1 .chart-title-footer text").text(languageData['chart-date-title']);
    $("#chd-container-footer-ch2 .chart-title-footer text").text(languageData['chart-month-title']);
    $("#chd-container-footer-ch3 .chart-title-footer text").text(languageData['chart-day-title']);
    $("#chd-container-footer-ch4 .chart-title-footer text").text(languageData['chart-hour-title']);
    $("#chd-container-footer-ch5 .chart-title-footer text").text(languageData['chart-severity-title']);

    $($("label[for=flag_0]").text(languageData['flags-involvement-pedestrian']));
    $($("label[for=flag_1]").text(languageData['flags-involvement-passenger-car']));
    $($("label[for=flag_2]").text(languageData['flags-involvement-cyclist']));
    $($("label[for=flag_3]").text(languageData['flags-involvement-motorcycle']));
    $($("label[for=flag_4]").text(languageData['flags-involvement-animal']));
    $($("label[for=flag_5]").text(languageData['flags-involvement-trucks']));

    $("#text-severity").html(languageData['severity-text']);

    $("#area-filter-list text").text(languageData['area-filter-list']);

    $("i.chart-filters-clean").prop('title', languageData['tooltip-clear-selection']);
    $("div.legend-selected").prop('title', languageData['tooltip-zoom-selected-data']);
    $("div.legend-unselected").prop('title', languageData['tooltip-zoom-unselected-data']);
    $("div.legend-out").prop('title', languageData['tooltip-zoom-out-data']);
    $(".chart-title-footer text").prop('title', languageData['tooltip-go-to-chart']);
    $("#draw-polygon").prop('title', languageData['tooltip-sketch-new-polygon']);
    $("#delete-polygon").prop('title', languageData['tooltip-delete-polygon']);
    $(".chart-title text").prop('title', languageData['tooltip-open-close-chart']);
    $(".chart-drag-handle i").prop('title', languageData['tooltip-click-drag-chart']);

    $(".y-axis-label").text(languageData['charts-y-axis-detections']);

    $("#records_loaded").html(languageData['loading-data-records']);

    $("div.ii i").tooltipster('content', languageData['tooltipster-content']);

    $("#wgl-point-win").remove();
    $("#triangle").remove();
    setPointSelection(languageData);

    const ch2_x_labels = $("#ch2").find(".x.axis text");
    const monthOfTheYearLabels = languageData['month-of-the-year-labels'];
    for(let i=0; i<ch2_x_labels.length; i++) {
        $(ch2_x_labels[i]).text(monthOfTheYearLabels[i]);
    }

    const ch3_x_labels = $("#ch3").find(".x.axis text");
    const dayOfTheWeekLabels = languageData['day-of-the-week-labels'];
    for(let i=0; i<ch3_x_labels.length; i++) {
        $(ch3_x_labels[i]).text(dayOfTheWeekLabels[i]);
    }

    const ch5_x_labels = $("#ch5").find(".x.axis text");
    const severityLabels = languageData['severity-labels'];
    for(let i=0; i<ch5_x_labels.length; i++) {
        $(ch5_x_labels[i]).text(severityLabels[i]);
    }

    if($(".link-permalink").length > 0) {
        $(".link-permalink").trigger("permalink:change");
    }
}