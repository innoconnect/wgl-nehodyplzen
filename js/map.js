let map;
let draw;
let mapColor = 'dark-v9';
let polygons = [];
let control_options;
$(document).ready(function () {
    let zoom = getUrlParameter("zoom");
    let center = getUrlParameter("center");

    mapboxgl.accessToken = 'pk.eyJ1IjoiamlyaS1iIiwiYSI6ImNqZmNjajc1MTJjN2cyeG5ycG5lcWhpNHMifQ.d-4wK9BUDPUHq_SRgHYe9g';
    map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/dark-v9', // stylesheet location 'mapbox://styles/mapbox/dark-v9'
        center: (center === "" ? [13.37, 49.73] : center.split(",")), // starting position [lng, lat]
        zoom: (zoom === "" ? 10 : zoom), // starting zoom
        preserveDrawingBuffer: true
    });

    draw = new MapboxDraw({
        displayControlsDefault: false,
        controls: {
            polygon: true,
            trash: true
        }
    });
    map.addControl(draw, 'top-left');

    control_options = {"draw": draw.modes.DRAW_POLYGON, "delete": draw.modes.SIMPLE_SELECT};
});

function transform(x, y) {
    const tl = getTopLeftTC();
    const p = new mapboxgl.LngLat(y, x);
    const v = map.project(p);
    return toLevel0(v, tl, map.getZoom());
}
function toLevel0(pt, tl, zoom) {
    const scale = Math.pow(2, zoom);
    pt.x = pt.x / scale + tl.x;
    pt.y = pt.y / scale + tl.y;
    return pt;
}
function toggleColorScheme(element) {
    const schemes = element.parentNode.childNodes;
    for (let scheme in schemes) {
        if (schemes[scheme].nodeName === "SPAN") {
            if (schemes[scheme] === element) {
                schemes[scheme].classList.add("color-scheme-selected");
            } else {
                schemes[scheme].classList.remove("color-scheme-selected");
            }
        }
    }
    WGL.colorSchemes.setSchemeSelected($(element).data("scheme"));
    this.changeMapColor();
}

function changeMapColor() {
    const newMapColor = WGL.colorSchemes.getSchemeBgSelected();
    if(newMapColor !== mapColor) {
        map.setStyle('mapbox://styles/mapbox/' + newMapColor);
        mapColor = newMapColor;
    }
}

function toggleControl(element) {
    const options = element.parentNode.childNodes;
    for (let control in options) {
        if (options[control].nodeName === "BUTTON") {
            if (options[control] === element) {
                if(element.classList.contains("btn-selected")) {
                    options[control].classList.remove("btn-selected");
                    draw.changeMode(draw.modes.SIMPLE_SELECT);
                    if(WGL.getDimension('themap').getVisible()) {
                        const idt = WGL.getDimension('idt');
                        idt.setEnabled(true);
                    }
                } else {
                    if(element.value === 'draw') {
                        options[control].classList.add("btn-selected");
                        draw.changeMode(control_options[element.value]);
                        /*const idt = WGL.getDimension('idt');
                        idt.setEnabled(false);
                        $("#wgl-win-close").click();*/
                    } else {
                        const features = draw.getSelected().features;
                        for(let feature in features) {
                            if(features.hasOwnProperty(feature)) {
                                draw.delete(features[feature].id);
                                map.fire('draw.delete');
                            }
                        }
                        /*if(WGL.getDimension('themap').getVisible()
                            && draw.getAll().features.length === 0) {
                            const idt = WGL.getDimension('idt');
                            idt.setEnabled(true);
                        }*/
                    }
                }
            } else {
                options[control].classList.remove("btn-selected");
            }
        }
    }
}