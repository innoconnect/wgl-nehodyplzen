function DataLoader() {

  /**
   * Load text file
   */
  $("#speed_chart").text("Please wait... data are being loaded. This may take a while.");

  $("#records_loaded, #wgl, #OpenLayers_Control_Attribution_7")
    .removeClass("text_map_light")
    .removeClass("text_map_dark")
    .addClass("text_map_"+mapColor);

  this.loadPosData = function(file) {

    let pts = [];
    let days = [];
    let hours = [];
    let hours_full = [];
    let months =[];
    let crimes_per_month = [];
    let date = [];
    let severity = [];
    let pts_id = [];
    let flags = [];

    let hoursArray = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
    let weekday = ["Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday","Saturday"];
    let weekEnum = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday","Saturday", "Sunday"];
    let monthsArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    let dateArray = [
      "04-2013",
      "05-2013",
      "06-2013",
      "07-2013",
      "08-2013",
      "09-2013",
      "10-2013",
      "11-2013",
      "12-2013",
      "01-2014",
      "02-2014",
      "03-2014",
      "04-2014",
      "05-2014",
      "06-2014",
      "07-2014",
      "08-2014",
      "09-2014",
      "10-2014",
      "11-2014",
      "12-2014",
      "01-2015",
      "02-2015",
      "03-2015",
      "04-2015",
      "05-2015",
      "06-2015",
      "07-2015",
      "08-2015",
      "09-2015",
      "10-2015",
      "11-2015",
      "12-2015",
      "01-2016",
      "02-2016",
      "03-2016",
      "04-2016",
      "05-2016",
      "06-2016",
      "07-2016",
      "08-2016",
      "09-2016",
      "10-2016",
      "11-2016",
      "12-2016",
      "01-2017",
      "02-2017",
      "03-2017",
      "04-2017",
      "05-2017",
      "06-2017",
      "07-2017",
      "08-2017"
    ];

    let j = 0;

      let formatDate = d3.time.format("%Y-%m-%d %H:%M:%S");

    /**
     * load data
     */
    d3.csv(file, function(error, data) {

      let dateminmax;

      data.forEach(function (val, i) {

        pts[j++] = parseFloat(val.x);
        pts[j++] = parseFloat(val.y);

        pts_id[i] = i;

        let d = formatDate.parse(val["od"]);

        days[i] =  weekday[d.getDay()];

        months[i] = monthsArray[d.getMonth()];

        crimes_per_month[i] = ('0'+(d.getMonth()+1)).slice(-2)+"-"+d.getFullYear();

        hours[i] = d.getHours();
        hours_full[i] = d.getHours() + d.getMinutes()/60;
        date[i] = Math.round(d.getTime()/(1000*60*60));
        dateminmax = getMinMax(date[i], dateminmax);
        flags[i] = val["ts_flags"].slice(1,-1).split(",");

        if(val["ts_flags"].indexOf("death") !== -1) {
          severity[i] = "fatal";
        } else if(val["ts_flags"].indexOf("injury") !== -1) {
          severity[i] = "injuries";
        } else {
          severity[i] = "no injury";
        }
      });

        let start_date = formatDate.parse(data[0]['od']).toLocaleDateString('en-US', {year: 'numeric', month: 'long', day: 'numeric'});
        let end_date = formatDate.parse(data[data.length-1]['od']).toLocaleDateString('en-US', {year: 'numeric', month: 'long', day: 'numeric'});

      visualize({
        pts: pts,
        pts_id: pts_id,
        num: data.length,
        days: days,
        hours: hours,
        hours_full: hours_full,
        hoursEnum: hoursArray,
        months: months,
        date: crimes_per_month,
        dateEnum: dateArray,
        daysarray: weekEnum,
        monthsArray: monthsArray,
        severity: severity,
        severityEnum: ["no injury", "injuries", "fatal"],
          flagsArray: flags,
          flagsEnum: [
              "pedestrian_involvement",
              "personal_cars",
              "cyclist",
              "motocycle",
              "animal_hit",
              "trucks"
              ],
          flagsNames: [
              "pedestrian",
              "passenger car",
              "cyclist",
              "motorcycle",
              "animal",
              "truck"
          ],
        start_date: start_date,
        end_date: end_date,
        dmm: dateminmax
      });

    }).on("progress", function() {
      if (d3.event.lengthComputable) {
        let percentComplete = Math.round(d3.event.loaded * 100 / d3.event.total);
        $(".progress-inner").css("width", percentComplete + "%");
        $("#records_loaded b").html(numberWithSpaces(d3.event.target.responseText.split("\n").length));
      }
    });
  };

  this.loadLanguageData = function(file) {
    d3.json(file, function(data) {
      renderLanguage(data);
    });
  };

  function getMinMax(val, minmax){
    if (typeof(minmax)==='undefined'){
      minmax = [];
      minmax.min = Number.MAX_VALUE;
      minmax.max = Number.MIN_VALUE;
    }
    if (val < minmax.min) {minmax.min = val}
    if (val > minmax.max) {minmax.max = val}
    return minmax;
  }

}
