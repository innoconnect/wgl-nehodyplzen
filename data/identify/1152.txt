ID,od,popis,x,y,ts_flags
11520,2016-12-02 08:00:00,Od 2.12.2016 08:00 do 10:05; na silnici 18050 u obce Město Touškov okres Plzeň-sever; nehoda; 2 x OA bez zranění. PČR na místo.,137.435066312424,87.0688860568336,'{accindet,personal_cars}'
11521,2016-12-02 08:50:00,Od 2.12.2016 08:50 do 10:55; na silnici 27 u obce Horní Lukavice okres Plzeň-jih; nehoda; probíhají záchranné a vyprošťovací práce; komunikace dočasně uzavřena; 2 x OA se zraněním. IZS a PČR na místo. Odklon dopravy na Chlumčany a Dobřany.,137.476612267986,87.240989252986,'{accindet,injury,personal_cars}'
11522,2016-12-02 11:50:00,Od 2.12.2016 11:50 do 13:55; v ulici Karlovarská v obci Plzeň; nehoda; 2 x OA bez zranění. PČR na místo.,137.504314312434,87.0831386462365,'{accindet,personal_cars}'
11523,2016-12-02 12:00:00,Od 2.12.2016 12:00 do 14:05; v ulici Nádražní v obci Nepomuk okres Plzeň-jih; nehoda; 2 havarovaná vozidla; 2 x OA bez zranění. PČR na místo.,137.661255823567,87.3862743824697,'{accindet,personal_cars}'
11524,2016-12-02 14:20:00,Od 2.12.2016 14:20 do 16:25; v ulici Borská v obci Plzeň; nehoda; 2 x OA bez zranění. PČR na místo.,137.492674134655,87.1139825523022,'{accindet,personal_cars}'
11525,2016-12-02 14:25:00,Od 2.12.2016 14:25 do 16:30; v ulici Sirková v obci Plzeň; nehoda; 2 x OA bez zranění. PČR na místo.,137.519030756881,87.1034600323673,'{accindet,personal_cars}'
11526,2016-12-02 15:25:00,Od 2.12.2016 15:25 do 17:30; v ulici Doudlevecká v obci Plzeň; nehoda; 2 x OA bez zranění. PČR na místo.,137.514247823547,87.1106938686704,'{accindet,personal_cars}'
11527,2016-12-02 16:15:00,Od 2.12.2016 16:15 do 19:20; v ulici Němejcova v obci Plzeň; nehoda 2 autobusů; 2 x trolejbus bez zranění. PČR na místo.,137.505344001323,87.107647061958,{accindet}
11528,2016-12-02 17:40:00,Od 2.12.2016 17:40 do 19:45; na silnici 27 u obce Dolní Lukavice okres Plzeň-jih; nehoda; 2 x OA se zraněním. PČR na místo. IZS na místo.,137.477332623541,87.2687550389657,'{accindet,injury,personal_cars}'
11529,2016-12-02 18:30:00,Od 2.12.2016 18:30 do 20:35; v ulici Domažlická v obci Plzeň; nehoda; 2 x OA bez zranění. PČR na místo.,137.498621156878,87.1014857972732,'{accindet,personal_cars}'
