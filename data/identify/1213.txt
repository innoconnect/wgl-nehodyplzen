ID,od,popis,x,y,ts_flags
12130,2017-02-01 14:20:00,Od 1.2.2017 14:20 do 17:25; v ulici Obchodní v obci Plzeň; nehoda nákladního vozidla; 2 havarovaná vozidla; probíhá vyšetřování nehody; NA x OA bez zranění. Na místě PČR.,137.474134756874,87.1221907246566,'{accindet,personal_cars,trucks}'
12131,2017-02-01 15:10:00,Od 1.2.2017 15:10 do 17:15; v ulici Prokopova v obci Plzeň; nehoda; 2 havarovaná vozidla; probíhá vyšetřování nehody; 2 x OA bez zranění. Na místě PČR.,137.513933512435,87.1047970564774,'{accindet,personal_cars}'
12132,2017-02-02 05:00:00,Od 2.2.2017 05:00 do 07:05; na silnici 26 u obce Zbůch okres Plzeň-sever; nehoda; Havárie dodávky. PČR na místo.,137.420616534645,87.1625278598336,{accindet}
12133,2017-02-02 05:15:00,Od 2.2.2017 05:15 do 07:20; na silnici 26 u obce Chotěšov okres Plzeň-jih; nehoda; Střet 2x OA. PČR na místo.,137.39241102353,87.189016004943,'{accindet,personal_cars}'
12134,2017-02-02 05:40:00,Od 2.2.2017 05:40 do 07:45; na silnici 230 u obce Stod okres Plzeň-jih; nehoda; Havárie OA se zraněním. IZS a PČR na místo.,137.341720890189,87.2035551110242,'{accindet,injury,personal_cars}'
12135,2017-02-02 05:45:00,Od 2.2.2017 05:45 do 07:50; na silnici 18033 u obce Dobřany okres Plzeň-jih; nehoda; Střet 2x OA. PČR na místo.,137.481572979098,87.2111400216294,'{accindet,personal_cars}'
12136,2017-02-02 06:05:00,Od 2.2.2017 06:05 do 09:10; v ulici V Zahradách v obci Plzeň; nehoda autobusu; Střet OA x BUS se zraněním. IZS a PČR na místo.,137.541272890217,87.1410688034558,'{accindet,injury,personal_cars}'
12137,2017-02-02 06:10:00,'D5, mezi km 150 a 80, ve směru Praha, mrznoucí déšť, Od 02.02.2017 06:10 Do 02.02.2017 17:00, zvýšené nebezpečí nehody',136.912726756796,87.2088095279421,{accindet}
12138,2017-02-02 06:25:00,Od 2.2.2017 06:25 do 08:30; na silnici 180 u obce Chotěšov okres Plzeň-jih; nehoda; Havárie OA. PČR na místo.,137.416211201311,87.197525734262,'{accindet,personal_cars}'
12139,2017-02-02 06:30:00,Od 2.2.2017 06:30 do 09:35; na silnici 18032 u obce Plzeň; nehoda autobusu; Střet BUS x OA. PČR na místo. IZS na místo.,137.518543645769,87.1391421461502,'{accindet,personal_cars}'
