ID,od,popis,x,y,ts_flags
8500,2015-12-18 17:30:00,'Od 18.12.2015 17:30 do 18:30; na silnici 180 u obce Dobřany okres Plzeň-jih; směr Chotěšov; nehoda; probíhají záchranné a vyprošťovací práce, nebezpečí; 2 x OA se zraněním.',137.434817423536,87.2009958120793,'{accindet,injury,personal_cars}'
8501,2015-12-18 18:50:00,'Od 18.12.2015 18:50 do 20:50; na silnici 180 u obce Dobřany okres Plzeň-jih; silnice uzavřena, nehoda; probíhají odklízecí práce; z důvodu odstraňování DN.',137.430779023535,87.1997380859368,{accindet}
8502,2015-12-18 19:15:00,Od 18.12.2015 19:15 do 20:15; v ulici U Trati v obci Plzeň; nehoda; 2 x OA bez zranění.,137.508489956879,87.108188435766,'{accindet,personal_cars}'
8503,2015-12-19 02:15:00,'Od 19.12.2015 02:15 do 04:20; na silnici 11745 u obce Klášter okres Plzeň-jih; nehoda; havarované vozidlo; probíhá vyšetřování nehody; havárie OA bez zranění, na místě PČR.',137.67680213468,87.3647044969347,'{accindet,personal_cars}'
8504,2015-12-19 10:40:00,Od 19.12.2015 10:40 do 12:45; v ulici Folmavská v obci Plzeň; nehoda; 2 havarovaná vozidla; Střet 2x OA. PČR na místo.,137.487912534654,87.1182424663058,'{accindet,personal_cars}'
8505,2015-12-19 10:40:00,Od 19.12.2015 10:40 do 12:45; v ulici Sadová v obci Plzeň; nehoda; 3 havarovaná vozidla; Střet 3x OA. PČR na místo.,137.541626312439,87.0886558922098,'{accindet,personal_cars}'
8506,2015-12-19 11:40:00,Od 19.12.2015 11:40 do 13:45; v ulici Komenského v obci Plzeň; nehoda; Střet 2x OA. PČR na místo.,137.509101512435,87.0705829146288,'{accindet,personal_cars}'
8507,2015-12-19 14:15:00,Od 19.12.2015 14:15 do 16:20; v ulici Karlovarská v obci Plzeň; nehoda; 2 havarovaná vozidla; Střet 2x OA. PČR na místo.,137.494673779099,87.0723127511044,'{accindet,personal_cars}'
8508,2015-12-19 14:35:00,Od 19.12.2015 14:35 do 16:40; na silnici 233 u obce Plzeň; nehoda; Srážka OA s prasetem. PČR na místo.,137.568027023554,87.0726529860528,'{accindet,personal_cars,animal_hit}'
8509,2015-12-19 16:40:00,Od 19.12.2015 16:40 do 18:45; na silnici 20 u obce Všeruby okres Plzeň-sever; nehoda; 2 havarovaná vozidla; Střet 2x OA. PČR na místo.,137.411972979088,87.0105709948373,'{accindet,personal_cars}'
