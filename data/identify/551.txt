ID,od,popis,x,y,ts_flags
5510,2015-02-04 06:00:00,'Od 4.2.2015 06:00 do 08:05; na silnici 178 v obci Řenče okres Plzeň-jih; nehoda; 2x OA bez zranění, průjezdné',137.523374223548,87.2730070244939,'{accindet,personal_cars}'
5511,2015-02-04 06:35:00,'Od 4.2.2015 06:35 do 08:40; v ulici Na Roudné v obci Plzeň; nehoda; 2x OA bez zranění, průjezdné',137.519550579103,87.0838718167271,'{accindet,personal_cars}'
5512,2015-02-04 06:35:00,'Od 4.2.2015 06:35 do 08:40; v ulici Masarykova v obci Plzeň; nehoda; probíhá vyšetřování nehody, nebezpečí; 2 x OA bez zranění, průjezdné s opatrností',137.54116622355,87.0922893455006,'{accindet,personal_cars}'
5513,2015-02-04 07:00:00,'Od 4.2.2015 07:00 do 09:05; v ulici Plzeňská v obci Třemošná okres Plzeň-sever; nehoda; 2x OA bez zranění, průjezdné',137.524384001326,87.0268096312476,'{accindet,personal_cars}'
5514,2015-02-04 08:10:00,'Od 4.2.2015 08:10 do 09:10; na silnici v obci Plzeň; nehoda; probíhají záchranné a vyprošťovací práce, nebezpečí; 2 OA zraněná řidička',137.531811556882,87.1175075673828,'{accindet,injury,personal_cars}'
5515,2015-02-04 08:35:00,Od 4.2.2015 08:35 do 09:35; v ulici Rokycanská v obci Plzeň; nehoda nákladního vozidla; srážka OA s NA,137.530742045771,87.0984769841982,'{accindet,personal_cars}'
5516,2015-02-04 10:10:00,Od 4.2.2015 10:10 do 11:10; v ulici Borská v obci Plzeň; nehoda; sražená chodkyně OA,137.494121245766,87.1132618979099,'{accindet,personal_cars}'
5517,2015-02-04 14:30:00,Od 4.2.2015 14:30 do 15:30; na silnici u obce Líté okres Plzeň-sever; nehoda; 2 x OA,137.409791290199,86.9181802173009,'{accindet,personal_cars}'
5518,2015-02-04 14:55:00,Od 4.2.2015 14:55 do 15:55; na silnici 27 u obce Plasy okres Plzeň-sever; nehoda; havárie OA,137.520146490214,86.8854537896967,'{accindet,personal_cars}'
5519,2015-02-04 15:20:00,Od 4.2.2015 15:20 do 16:20; na silnici v obci Plzeň; nehoda; srážka 2 OA,137.530882845771,87.1169079791825,'{accindet,personal_cars}'
