ID,od,popis,x,y,ts_flags
6960,2015-07-20 13:45:00,'Od 20.7.2015 14:45 do 15:50; v ulici Mikulášská v obci Plzeň; nehoda autobusu; OA x autobus bez zranění, na místo PČR.',137.518931912436,87.1049170018597,'{accindet,personal_cars}'
6961,2015-07-20 14:10:00,'Od 20.7.2015 15:10 do 16:15; v ulici Částkova v obci Plzeň; nehoda, silný provoz; 3 x OA bez zranění, na místo PČR.',137.529400179104,87.1150222579126,'{accindet,personal_cars}'
6962,2015-07-20 14:15:00,'Od 20.7.2015 15:15 do 16:20; v ulici Doudlevecká v obci Plzeň; nehoda; 2 x OA bez zranění, na místo PČR.',137.513676090213,87.1126512594764,'{accindet,personal_cars}'
6963,2015-07-20 22:15:00,'Od 20.7.2015 23:15 do 21.7.2015 00:20; v ulici Edvarda Beneše v obci Plzeň; nehoda; 2 havarovaná vozidla; probíhá vyšetřování nehody; 2 x OA bez zranění, na místě PČR.',137.512420267991,87.120107171925,'{accindet,personal_cars}'
6964,2015-07-21 02:40:00,'Od 21.7.2015 03:40 do 04:40; na silnici u obce Nezbavětice okres Plzeň-město; nehoda; havarované vozidlo; probíhá vyšetřování nehody; havárie Dodávky do příkopu, bez zranění osob. Na místě PČR.',137.577715912444,87.2026928716322,{accindet}
6965,2015-07-21 07:05:00,'Od 21.7.2015 08:05 do 10:10; na silnici třídy III u obce Plzeň; nehoda; 2xOA bez zranění, bez únku PHM, zvýšená opatrnost',137.502846579101,87.1412535477652,{accindet}
6966,2015-07-21 08:00:00,'Od 21.7.2015 09:00 do 11:05; v ulici Čechova v obci Plzeň; nehoda; 2xOA bez zranění, bez úniku PHM, zvýšená opatrnost',137.509666845768,87.114773610582,{accindet}
6967,2015-07-21 10:05:00,'Od 21.7.2015 11:05 do 13:10; v ulici Slovanská v obci Plzeň; Slovanská x Sladkovského; nehoda; 2xOA bez zranění bez úniku PHM, překáží provozu',137.520549690214,87.1109953489388,{accindet}
6968,2015-07-21 11:15:00,'Od 21.7.2015 12:15 do 14:20; v ulici Hřímalého v obci Plzeň; nehoda; 2xOA bez zranění, bez úniku PHMN, zvýšená patrnost',137.507103290212,87.1095099369869,{accindet}
6969,2015-07-21 12:20:00,'Od 21.7.2015 13:20 do 15:25; v ulici Ke Kukačce v obci Plzeň; nehoda; 2xOA bez zranění, bez úniku PHM. zvýšená opatrost, ohrožení BESIP',137.540900979106,87.0990657740596,{accindet}
