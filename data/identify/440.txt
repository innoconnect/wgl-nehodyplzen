ID,od,popis,x,y,ts_flags
4400,2014-09-29 10:40:00,'Od 29.9.2014 11:40 do 13:45; na silnici 20148 v obci Nečtiny okres Plzeň-sever; nehoda; 2xOA bez zranění, bez úniku PHM, zvýšená opatrnost',137.314666667963,86.8524131304507,{accindet}
4401,2014-09-29 14:20:00,'Od 29.9.2014 15:20 do 17:25; v ulici Ve Višňovce v obci Plzeň; nehoda; OA x dodávka bez zranění, bez úniku PHM',137.548359823551,87.114775811006,'{accindet,personal_cars}'
4402,2014-09-29 14:55:00,'Od 29.9.2014 15:55 do 18:00; v ulici Slovanská alej v obci Plzeň; nehoda; 2xOA bez zranění, překáží provozu, zvýšená opatrnost.',137.535321601327,87.1220565172681,{accindet}
4403,2014-09-29 15:55:00,'Od 29.9.2014 16:55 do 19:00; na silnici 191 u obce Mladý Smolivec okres Plzeň-jih; nehoda; 1xOA se zraněním, na místo RZS a PČR.',137.783629512473,87.3362116251076,'{accindet,injury}'
4404,2014-09-29 17:05:00,'Od 29.9.2014 18:05 do 20:10; v ulici Jízdecká v obci Plzeň; Jízdecká x Pobřežní; nehoda; DN motocyklista se zraněním, na místo PČR a RZS.',137.509018312435,87.0969879221678,'{accindet,injury}'
4405,2014-09-29 18:10:00,'Od 29.9.2014 19:10 do 21:15; na silnici v obci Losiná okres Plzeň-město; nehoda; 2xOA bez zranění, bez úniku PHM, částečně průjezdné, zvýšená opatrnost.',137.559818667997,87.1756268626705,{accindet}
4406,2014-09-29 18:40:00,'Od 29.9.2014 19:40 do 21:45; na silnici 180 u obce Útušice okres Plzeň-jih; nehoda; OA x srna, na misto hlídka PČR',137.510961067991,87.1883523956494,'{accindet,personal_cars,animal_hit}'
4407,2014-09-30 05:45:00,'Od 30.9.2014 06:45 do 08:50; na silnici 230 u obce Žinkovy okres Plzeň-jih; nehoda; OA x srna, zvýšená opatrnost, ohrožení BESIP',137.598075023558,87.3698029602033,'{accindet,personal_cars,animal_hit}'
4408,2014-09-30 07:00:00,'Od 30.9.2014 08:00 do 10:05; v ulici Přemyslova v obci Plzeň; nehoda; 3 havarovaná vozidla; 3 x OA bez zranění, průjezdné',137.506500979101,87.0984219567765,'{accindet,personal_cars}'
4409,2014-09-30 07:55:00,'Od 30.9.2014 08:55 do 11:00; v ulici Částkova v obci Plzeň; naproti čerpací stanici Agip; nehoda; 2x OA bez zranění, průjezdné',137.529878756882,87.1146899944054,'{accindet,personal_cars}'
